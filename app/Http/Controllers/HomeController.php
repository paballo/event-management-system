<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use DB;

use App\Models\Event;

use App\Models\Booking;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }

    public function booking_view(){
        return view('booking');
      }

      public function booking_create(Request $request,){
        $request->validate([
          'event_id' => 'required',
          'name' => 'required',
          'email' => 'required',
          'contact' => 'required',
          
      ]);

      $input = $request->all();

      Booking::create($input);

      $event = Event::all();
   
      return view('/booking_msg');
      }  

      public function booking($id){
        $event = Event::find($id);
        return view('/booking', compact('event'));
      } 

      public function event_view(){
      $event = Event::all();
      return view('event', compact('event'));
     
      }

      public function event_details($id){
        $event = Event::find($id);
        $shareComponent = \Share::page(
          '',
          'Your share text comes here',
      )
      ->facebook()
      ->twitter()
      ->linkedin()
      ->telegram()
      ->whatsapp()        
      ->reddit();
      
        return view('/event_details', compact('event','shareComponent'));
         
        }

        public function booking_msg(){
          return view('booking_msg');
        }

       

}
