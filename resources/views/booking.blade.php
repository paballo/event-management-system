<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>Booking - {{$event->event}}</title>
	<link rel="shortcut icon" href="assets/images/favicon.png" type="image/png">

<link rel="stylesheet" href="https://preview.uideck.com/items/eventify/assets/css/bootstrap.min.css">

<link rel="stylesheet" href="https://preview.uideck.com/items/eventify/assets/css/flaticon.css">

<link rel="stylesheet" href="https://preview.uideck.com/items/eventify/assets/css/LineIcons.css">

<link rel="stylesheet" href="https://preview.uideck.com/items/eventify/assets/css/animate.css">

<link rel="stylesheet" href="https://preview.uideck.com/items/eventify/assets/css/magnific-popup.css">

<link rel="stylesheet" href="https://preview.uideck.com/items/eventify/assets/css/slick.css">

<link rel="stylesheet" href="https://preview.uideck.com/items/eventify/assets/css/default.css">

<link rel="stylesheet" href="https://preview.uideck.com/items/eventify/assets/css/style.css">
</head>

<body>
	

	<section id="contact" class="contact-area pt-80 pb-130">
		<div class="container">
		<div class="row">
		<div class="col-lg-4">
		<div class="contact-info pt-40">
		<div class="section-title pb-10">
		<h2 class="title">Get In Touch</h2>
		</div> 
		<ul>
		<li>
		<div class="single-info d-flex mt-25">
		<div class="info-icon">
		<i class="lni-envelope"></i>
		</div>
		<div class="info-content media-body">
		<h6 class="info-title">Email address</h6>
		<p class="text"><a href="/cdn-cgi/l/email-protection" class="__cf_email__" data-cfemail="cba8a4a5bfaaa8bf8bb2a4beb9a6aaa2a7e5a8a4a6">[email&#160;protected]</a></p>
		</div>
		</div> 
		</li>
		<li>
		<div class="single-info d-flex mt-25">
		<div class="info-icon">
		<i class="lni-phone-handset"></i>
		</div>
		<div class="info-content media-body">
		<h6 class="info-title">Phone Number</h6>
		<p class="text">+831 546 547</p>
		</div>
		</div> 
		</li>
		<li>
		<div class="single-info d-flex mt-25">
		<div class="info-icon">
		<i class="lni-money-location"></i>
		</div>
		<div class="info-content media-body">
		<h6 class="info-title">Location</h6>
		<p class="text">Cesare Rosaroll, 118 80139 Eventine</p>
		</div>
		</div> 
		</li>
		</ul>
		</div> 
		</div>
		<div class="col-lg-8">
		<div class="contact-form pt-20">
		<form action="/booking" method="POST" >
			@csrf
         <input type="hidden" class="form-control" name="event_id" value="{{$event->id}}" required>
		<div class="row">
		<div class="col-md-6">
		<div class="single-form">
		<input type="text" name="name" placeholder="Your Name">
		<i class="lni-user"></i>
		</div> 
		</div>
		<div class="col-md-6">
		<div class="single-form">
		<input type="email" name="email" placeholder="Your Email">
		<i class="lni-envelope"></i>
		</div> 
		</div>
		
		<div class="col-md-6">
		<div class="single-form" style="width: 210%">
		<input type="text" name="contact" placeholder="Phone Number">
		<i class="lni-phone-handset"></i>
		</div> 
		</div>
		<div class="col-md-12">
		<div class="single-form">
		<textarea name="address" placeholder="Address"></textarea>
		<i class="lni-comment-alt"></i>
		</div> 
		</div>
		<p class="form-message"></p>
		<div class="col-md-12">
		<div class="single-form">
		<button type="submit" class="main-btn main-btn-2">Send </button>
		</div> 
		</div>
		</div> 
		</form>
		</div> 
		</div>
		</div>  
		</div> 
		</section>
	
</body>

	
</html>