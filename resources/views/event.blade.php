
<!DOCTYPE html>
<html lang="en">
     		<meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <title>Event Management System</title>
        <!-- Favicon-->
        <link rel="icon" type="image/x-icon" href="assets/img/favicon.ico" />
        <!-- Font Awesome icons (free version)-->
        <script src="https://use.fontawesome.com/releases/v5.13.0/js/all.js" crossorigin="anonymous"></script>
        <!-- Google fonts-->
        <link href="https://fonts.googleapis.com/css?family=Merriweather+Sans:400,700" rel="stylesheet" />
        <link href="https://fonts.googleapis.com/css?family=Merriweather:400,300,300italic,400italic,700,700italic" rel="stylesheet" type="text/css" />
        <!-- Third party plugin CSS-->
        <link href="admin/assets/css/jquery.datetimepicker.min.css" rel="stylesheet">
        <link href="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/magnific-popup.min.css" rel="stylesheet" />
        <!-- Core theme CSS (includes Bootstrap)-->
        <link href="admin/assets/vendor/bootstrap-datepicker/css/bootstrap-datepicker.css" rel="stylesheet" />
        <link href="{{ url('/css/styles.css') }}" rel="stylesheet">
         <link href="admin/assets/css/select2.min.css" rel="stylesheet">

         
    <!-- BOOTSTRAP CSS -->
    <link rel="stylesheet" href="http://themearth.com/demo/html/emeet/view/assets/libs/bootstrap/css/bootstrap.min.css" media="all"/>

<!-- FONT AWESOME -->
<link rel="stylesheet" href="http://themearth.com/demo/html/emeet/view/assets/libs/fontawesome/css/font-awesome.min.css" media="all"/>

<!-- FONT AWESOME -->
<link rel="stylesheet" href="http://themearth.com/demo/html/emeet/view/assets/libs/maginificpopup/magnific-popup.css" media="all"/>

<!-- Time Circle -->
<link rel="stylesheet" href="http://themearth.com/demo/html/emeet/view/assets/libs/timer/TimeCircles.css" media="all"/>

<!-- OWL CAROUSEL CSS -->
<link rel="stylesheet" href="http://themearth.com/demo/html/emeet/view/assets/libs/owlcarousel/owl.carousel.min.css" media="all" />
<link rel="stylesheet" href="http://themearth.com/demo/html/emeet/view/assets/libs/owlcarousel/owl.theme.default.min.css" media="all" />

<!-- GOOGLE FONT -->
<link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Oswald:400,700%7cPoppins:300,400,400i,600,600i,700,800,900"/>

<!-- MASTER  STYLESHEET  -->
<link id="lgx-master-style" rel="stylesheet" href="http://themearth.com/demo/html/emeet/view/assets/css/style-default.min.css" media="all"/>

<!-- MODERNIZER CSS  -->
<script src="http://themearth.com/demo/html/emeet/view/assets/js/vendor/modernizr-2.8.3.min.js"></script>

        <script src="admin/assets/vendor/jquery/jquery.min.js"></script>
        <script src="admin/assets/vendor/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>

    <script type="text/javascript" src="admin/assets/js/select2.min.js"></script>

    <style>
    	header.masthead {
		  background: url(admin/assets/uploads/1602641160_JSAV-multiscreen_3ddbdd40-38d9-4b64-9cf2-5d0ef356f29c.jpg);
		  background-repeat: no-repeat;
		  background-size: cover;
		}
    
  #viewer_modal .btn-close {
    position: absolute;
    z-index: 999999;
    /*right: -4.5em;*/
    background: unset;
    color: white;
    border: unset;
    font-size: 27px;
    top: 0;
}
#viewer_modal .modal-dialog {
        width: 80%;
    max-width: unset;
    height: calc(90%);
    max-height: unset;
}
  #viewer_modal .modal-content {
       background: black;
    border: unset;
    height: calc(100%);
    display: flex;
    align-items: center;
    justify-content: center;
  }
  #viewer_modal img,#viewer_modal video{
    max-height: calc(100%);
    max-width: calc(100%);
  }
  body, footer {
    background: #000000e6 !important;
}
 
    </style>
    <body id="page-top">
        <!-- Navigation-->
        <div class="toast" id="alert_toast" role="alert" aria-live="assertive" aria-atomic="true">
        <div class="toast-body text-white">
        </div>
      </div>
        <nav class="navbar navbar-expand-lg navbar-light fixed-top py-3" id="mainNav">
            <div class="container">
                <a class="navbar-brand js-scroll-trigger" href="./">Event Management System</a>
                <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation"><span class="navbar-toggler-icon"></span></button>
                
            </div>
        </nav>
       
        <style>
#portfolio .img-fluid{
    width: calc(100%);
    height: 30vh;
    z-index: -1;
    position: relative;
    padding: 1em;
}
.event-list{
cursor: pointer;
}
span.hightlight{
    background: yellow;
}
.banner{
        display: flex;
        justify-content: center;
        align-items: center;
        min-height: 26vh;
        width: calc(30%);
    }
    .banner img{
        width: calc(100%);
        height: calc(100%);
        cursor :pointer;
    }
.event-list{
cursor: pointer;
border: unset;
flex-direction: inherit;
}

.event-list .banner {
    width: calc(40%)
}
.event-list .card-body {
    width: calc(60%)
}
.event-list .banner img {
    border-top-left-radius: 5px;
    border-bottom-left-radius: 5px;
    min-height: 50vh;
}
span.hightlight{
    background: yellow;
}
.banner{
   min-height: calc(100%)
}
</style>
        <header class="masthead">
            <div class="container-fluid h-100">
                <div class="row h-100 align-items-center justify-content-center text-center">
                    <div class="col-lg-8 align-self-end mb-4 page-title">
                    	<h3 class="text-white">Welcome to Event Management System</h3>
                        <hr class="divider my-4" />

                    <div class="col-md-12 mb-2 justify-content-center">
                    </div>                        
                    </div>
                    
                </div>
            </div>
        </header>
        <div class="container mt-3 pt-2">
            <h4 class="text-center text-white">Upcoming Events</h4>
            <hr class="divider">

            @foreach ($event as $item)
            <div class="card event-list" >
                <div class='banner'>
                   <img src="/uploads/avatars/{{ $item->image }}"/>
               </div>
               <div class="card-body">
                   <div class="row  align-items-center justify-content-center text-center h-100">
                       <div class="">
                           <h3><b class="filter-txt">{{$item->event}}</b></h3>
                           <div><small><p><b><i class="fa fa-calendar">{{$item->created_at}}</i> </b></p></small></div>
                           <div><small><p><b><i class="fa fa-clock"></i> {{$item->created_at}} </b></p></small></div>
                           <hr>
                           <larger class="truncate filter-txt"></larger>
                           <br>
                           <hr class="divider"  style="max-width: calc(80%)">
                           <a href="event_details/{{$item->id}}" class="btn btn-primary float-right read_more" >Read More</a>
                       </div>
                   </div>
                   

               </div>
           </div>
           <br>
            @endforeach
            
        </div>
            