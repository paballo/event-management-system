@extends('admin.layout')

@section('content')
<div class="col-lg-12">
	<div class="card">
		<div class="card-header">
			Event Audience Report
		</div>
		<div class="card-body">
			<div class="col-md-12">
				<form action="" id="filter">
					<div class="row form-group">
						<div class="col-md-4">
							
						</div>
						<form action="/admin/events/audience_report" method="GET">
							<div class="input-group mb-3">
							</div>

							<div class="col-md-2">
								<label for="" class="control-label">&nbsp;</label>
								<a href="/admin/events/view2" class="btn-primary btn-sm btn-block col-sm-12" type="submit">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-clipboard"></i>&nbsp;Ecexl</a>
							</div>
							<div class="col-md-2">
								<label for="" class="control-label">&nbsp;</label>
								<a href="/admin/events/view1" class="btn-success btn-sm btn-block col-sm-12" type="submit">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-print"></i>&nbsp;PDF</a>
							</div>
						
					</div>
				</form>
				<hr>
				<div class="row" id="printable">
					
					<table class="table table-bordered">
						<thead>
							<tr><th class="text-center">#</th>
							<th class="text-center">Name</th>
							<th class="text-center">Email</th>
							<th class="text-center">Contact</th>
							<th class="text-center">Payment Status</th>
						</tr></thead>
						<tbody>
							@foreach ($audience as  $audience)
							<tr>
								<th scope="row">{{ $audience->id }}</th>
								<td>{{ $audience->name }}</td>
								<td>{{ $audience->email }}</td>
								<td>{{ $audience->contact }}</td>
								<td>{{ $audience->cost}}</td>
							</tr>
							@endforeach
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection