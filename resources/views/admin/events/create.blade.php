@extends('admin.layout')

@section('content')

<link rel="stylesheet" href="https://unpkg.com/tailwindcss@2.1.4/dist/tailwind.min.css">
<!-- component -->
<div class="py-12">
    <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
        @if (session('status'))
                <h6 class="alert alert-success">{{ session('status') }}</h6>
            @endif
        <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
            <div class="p-6 bg-white border-b border-gray-200">
                <form enctype="multipart/form-data" method="POST" action="/admin/events/create">
                    @csrf
                    <div class="mb-4">
                        <label class="text-xl text-gray-600">Event <span class="text-red-500">*</span></label></br>
                        <input type="text" class="border-2 border-gray-300 p-2 w-full" name="event" id="title" value="" required>
                    </div>

                    <div class="mb-4">
                        <label class="text-xl text-gray-600">Host <span class="text-red-500">*</span></label></br>
                        <input type="text" class="border-2 border-gray-300 p-2 w-full" name="host" id="title" value="" required>
                    </div>

                    <div class="mb-4">
                        <label class="text-xl text-gray-600">Slug <span class="text-red-500">*</span></label></br>
                        <input type="text" class="border-2 border-gray-300 p-2 w-full" name="slug" id="title" value="" required>
                    </div>

                    <div class="mb-4">
                        <label class="text-xl text-gray-600">Schedule - Start <span class="text-red-500">*</span></label></br>
                        <input type="datetime-local" class="border-2 border-gray-300 p-2 w-full" name="starting"  placeholder="(Optional)">
                    </div>

                    <div class="mb-4">
                        <label class="text-xl text-gray-600">Schedule - End <span class="text-red-500">*</span></label></br>
                        <input type="datetime-local" class="border-2 border-gray-300 p-2 w-full" name="ending"  placeholder="(Optional)">
                    </div>

                    <div class="mb-4">
                        <label class="text-xl text-gray-600">Venue <span class="text-red-500">*</span></label></br>
                        <input type="text" class="border-2 border-gray-300 p-2 w-full" name="venue"  placeholder="venue">
                    </div>

                    <div class="mb-4">
                        <label class="text-xl text-gray-600">Category <span class="text-red-500">*</span></label></br>
                        
                        <select name="categories" class="border-2 border-gray-300 p-2 w-full">
                            <option value="1">Vacwork</option>
                            <option value="2">Hackathon</option>
                        </select>
                    </div>

                    <div class="mb-8">
                        <label class="text-xl text-gray-600">Description <span class="text-red-500">*</span></label></br>
                        <textarea name="description" id="description" class="border-2 border-gray-500">
                            
                        </textarea>
                    </div>
                    <div class="mb-4">
                      <input type="checkbox" name="cost" value= "1"> Private Event (Do not show in website)
                       <br>
                       <br>
                       <input type="checkbox" name="cost" value= "2"> Free For All
                       <br>
                       <br>
                       <label class="text-xl text-gray-600">Registration Fee <span class="text-red-500">*</span></label></br>
                       <input type="text" class="border-2 border-gray-300 p-2 w-full" name="cost"  placeholder="0">
                    </div>
                    <div class="mb-4">
                        <label class="text-xl text-gray-600">Audience Capacity <span class="text-red-500">*</span></label></br>
                        <input type="number" class="border-2 border-gray-300 p-2 w-full" name="audience"  placeholder="0">
                    </div>
                    <div class="mb-4">
                        <label class="text-xl text-gray-600">Banner Image <span class="text-red-500">*</span></label></br>
                        <input type="file" class="border-2 border-gray-300 p-2 w-full" name="image"  accept="image/*">
                    </div>
                    <div class="mb-4">
                        <label class="text-xl text-gray-600">Link to the event <span class="text-red-500">*</span></label></br>
                        <input type="text" class="border-2 border-gray-300 p-2 w-full" name="url"  placeholder="Url">
                    </div>
                    <div class="flex p-1">
                        <button role="submit" class="p-3 bg-blue-500 text-white hover:bg-blue-400" required>Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script src="https://cdn.ckeditor.com/4.16.0/standard/ckeditor.js"></script>

<script>
    CKEDITOR.replace( 'description' );
</script>

@endsection