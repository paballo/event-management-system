<title>Geekulcha Dashboard</title>

<link rel="shortcut icon" href="https://www.geekulcha.dev/favicon.ico" type="image/png" />

<link rel="stylesheet" href="https://unpkg.com/tailwindcss@1.9.6/dist/tailwind.min.css">
<link href="https://unpkg.com/pattern.css" rel="stylesheet">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">

<link rel="stylesheet" href="https://demo.dashboardpack.com/hospital-html/css/bootstrap.min.css" />
<!-- themefy CSS -->
<link rel="stylesheet" href="https://demo.dashboardpack.com/hospital-html/vendors/themefy_icon/themify-icons.css" />
<!-- swiper slider CSS -->
<link rel="stylesheet" href="https://demo.dashboardpack.com/hospital-html/vendors/swiper_slider/css/swiper.min.css" />
<!-- select2 CSS -->
<link rel="stylesheet" href="https://demo.dashboardpack.com/hospital-html/vendors/select2/css/select2.min.css" />
<!-- select2 CSS -->
<link rel="stylesheet" href="https://demo.dashboardpack.com/hospital-html/vendors/niceselect/css/nice-select.css" />
<!-- owl carousel CSS -->
<link rel="stylesheet" href="https://demo.dashboardpack.com/hospital-html/vendors/owl_carousel/css/owl.carousel.css" />
<!-- gijgo css -->
<link rel="stylesheet" href="https://demo.dashboardpack.com/hospital-html/vendors/gijgo/gijgo.min.css" />
<!-- font awesome CSS -->
<link rel="stylesheet" href="https://demo.dashboardpack.com/hospital-html/vendors/font_awesome/css/all.min.css" />
<link rel="stylesheet" href="https://demo.dashboardpack.com/hospital-html/vendors/tagsinput/tagsinput.css" />
<!-- datatable CSS -->
<link rel="stylesheet" href="https://demo.dashboardpack.com/hospital-html/vendors/datatable/css/jquery.dataTables.min.css" />
<link rel="stylesheet" href="https://demo.dashboardpack.com/hospital-html/vendors/datatable/css/responsive.dataTables.min.css" />
<link rel="stylesheet" href="https://demo.dashboardpack.com/hospital-html/vendors/datatable/css/buttons.dataTables.min.css" />
<!-- text editor css -->
<link rel="stylesheet" href="https://demo.dashboardpack.com/hospital-html/vendors/text_editor/summernote-bs4.css" />
<!-- morris css -->
<link rel="stylesheet" href="https://demo.dashboardpack.com/hospital-html/vendors/morris/morris.css">
<!-- metarial icon css -->
<link rel="stylesheet" href="https://demo.dashboardpack.com/hospital-html/vendors/material_icon/material-icons.css" />

<!-- menu css  -->
<link rel="stylesheet" href="https://demo.dashboardpack.com/hospital-html/css/metisMenu.css">

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<!-- style CSS -->
<link rel="stylesheet" href="https://demo.dashboardpack.com/hospital-html/css/style.css" />
<link rel="stylesheet" href="https://demo.dashboardpack.com/hospital-html/css/colors/default.css" id="colorSkinCSS">


<!-- component -->
<main class="flex-1 overflow-x-hidden overflow-y-auto bg-gray-200">
    <div class="mx-auto px-6 py-8">
    
    @yield('content')
    
    </div>