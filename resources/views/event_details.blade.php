<link rel="shortcut icon" href="https://www.geekulcha.dev/favicon.ico" type="image/png" />

<link rel="stylesheet" href="https://unpkg.com/tailwindcss@1.9.6/dist/tailwind.min.css">
<link href="https://unpkg.com/pattern.css" rel="stylesheet">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">

<link rel="stylesheet" href="https://demo.dashboardpack.com/hospital-html/css/bootstrap.min.css" />
<!-- themefy CSS -->
<link rel="stylesheet" href="https://demo.dashboardpack.com/hospital-html/vendors/themefy_icon/themify-icons.css" />
<!-- swiper slider CSS -->
<link rel="stylesheet" href="https://demo.dashboardpack.com/hospital-html/vendors/swiper_slider/css/swiper.min.css" />
<!-- select2 CSS -->
<link rel="stylesheet" href="https://demo.dashboardpack.com/hospital-html/vendors/select2/css/select2.min.css" />
<!-- select2 CSS -->
<link rel="stylesheet" href="https://demo.dashboardpack.com/hospital-html/vendors/niceselect/css/nice-select.css" />
<!-- owl carousel CSS -->
<link rel="stylesheet" href="https://demo.dashboardpack.com/hospital-html/vendors/owl_carousel/css/owl.carousel.css" />
<!-- gijgo css -->
<link rel="stylesheet" href="https://demo.dashboardpack.com/hospital-html/vendors/gijgo/gijgo.min.css" />
<!-- font awesome CSS -->
<link rel="stylesheet" href="https://demo.dashboardpack.com/hospital-html/vendors/font_awesome/css/all.min.css" />
<link rel="stylesheet" href="https://demo.dashboardpack.com/hospital-html/vendors/tagsinput/tagsinput.css" />
<!-- datatable CSS -->
<link rel="stylesheet" href="https://demo.dashboardpack.com/hospital-html/vendors/datatable/css/jquery.dataTables.min.css" />
<link rel="stylesheet" href="https://demo.dashboardpack.com/hospital-html/vendors/datatable/css/responsive.dataTables.min.css" />
<link rel="stylesheet" href="https://demo.dashboardpack.com/hospital-html/vendors/datatable/css/buttons.dataTables.min.css" />
<!-- text editor css -->
<link rel="stylesheet" href="https://demo.dashboardpack.com/hospital-html/vendors/text_editor/summernote-bs4.css" />
<!-- morris css -->
<link rel="stylesheet" href="https://demo.dashboardpack.com/hospital-html/vendors/morris/morris.css">
<!-- metarial icon css -->
<link rel="stylesheet" href="https://demo.dashboardpack.com/hospital-html/vendors/material_icon/material-icons.css" />

<!-- menu css  -->
<link rel="stylesheet" href="https://demo.dashboardpack.com/hospital-html/css/metisMenu.css">

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<!-- style CSS -->
<link rel="stylesheet" href="https://demo.dashboardpack.com/hospital-html/css/style.css" />
<link rel="stylesheet" href="https://demo.dashboardpack.com/hospital-html/css/colors/default.css" id="colorSkinCSS">

<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css"/>

<style >
	.imgs{
		margin: .5em;
		max-width: calc(100%);
		max-height: calc(100%);
	}
	.imgs img{
		max-width: calc(100%);
		max-height: calc(100%);
		cursor: pointer;
	}
	#imagesCarousel,#imagesCarousel .carousel-inner,#imagesCarousel .carousel-item{
		height: 40vh !important;background: black;

	}
	#imagesCarousel{
		margin-left:unset !important ;
	}
	#imagesCarousel .carousel-item.active{
		display: flex !important;
	}
	#imagesCarousel .carousel-item-next{
		display: flex !important;
	}
	#imagesCarousel .carousel-item img{
		margin: auto;
		margin-top: unset;
		margin-bottom: unset;
	}
	#imagesCarousel img{
		width: calc(100%)!important;
		height: auto!important;
		/*max-height: calc(100%)!important;*/
		max-width: calc(100%)!important;
		cursor :pointer;
	}
	#banner{
		display: flex;
		justify-content: center;
	}
	#banner img{
		max-width: calc(100%);
		max-height: 50vh;
		cursor :pointer;
	}
	div#social-links {
                margin: 0 auto;
                max-width: 500px;
				
            }
            div#social-links ul li {
                display: inline-block;
            }          
            div#social-links ul li a {
                padding: 20px;
                border: 1px solid #ccc;
                margin: 1px;
                font-size: 20px;
                color: #222;
                background-color: #ccc;
            }
</style>
<div class="container-field">
	<div class="col-lg-12">
		<div class="card">
			<div class="card-body">
				<div class="row">
					<div class="col-md-12">
						<div id="banner" class='mx-2'>
								<img src="/uploads/avatars/{{ $event->image }}" alt="">
						</div>
					<br>
					</div>
					<div class="col-md-12">
						<h4 class="text-center"><b>{{ $event->event}}</b></h4>
						<p class="text-center"><small><b><i>Venue : {{ $event->venue}}</small></i></b></p>
						<hr class="divider" style="max-width:calc(100%)">
					</div>
					<div class="col-md-12" id="content">
						
						<div id="imagesCarousel" class="carousel slide col-sm-4 float-left  ml-0 mx-4"  data-ride="carousel">

							  <div class="carousel-inner">
							  
						
					  	 <a class="carousel-control-prev" href="#imagesCarousel" role="button" data-slide="prev">
						    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
						    <span class="sr-only">Previous</span>
						  </a>
						  <a class="carousel-control-next" href="#imagesCarousel" role="button" data-slide="next">
						    <span class="carousel-control-next-icon" aria-hidden="true"></span>
						    <span class="sr-only">Next</span>
						  </a>
					  		</div>
					  		
						</div>
					<p class="">
						
						<p><b><i class="fa fa-calendar"></i> {{ \Carbon\Carbon::parse($event->starting)->format(' H.i  A  d-m-y ') }}</b>&nbsp;&nbsp;&nbsp;&nbsp;<b><i class="fa fa-calendar"></i> {{ \Carbon\Carbon::parse($event->ending)->format(' H.i  A  d-m-y ') }}</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b class="px-2 py-1 font-semibold text-white text-700  {{$event->status=='1' ? 'btn-info  rounded-sm' : 'bg-danger rounded-sm'}}  "> {{ $event->categories=='1' ? 'Vacwork' : 'Hackathon' }} </b></p>
						{!!$event->description !!}
                         <br>
                        <ul class="header-btn">
                            <li><a class="btn btn-danger center" href="/booking/{{$event->id}}">Register Now</a>&nbsp; &nbsp; &nbsp; &nbsp;<a href="#" type="button" class="btn btn-info">Learn More</a></li>
                        </ul>
						<br>
						<div class="container mt-4">
							<h2 class="mb-5 text-center">Social Share </h2>
							<div class="mb-5 text-center">{!! $shareComponent !!}</div>
						</div>
					</p>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script>
	$('#imagesCarousel img,#banner img').click(function(){
		viewer_modal($(this).attr('src'))
	})
	$('.carousel').carousel()
</script>


