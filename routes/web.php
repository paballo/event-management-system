<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers\AdminController;

use App\Http\Controllers\HomeController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

//event

Route::get('/admin/events/create', [AdminController::class, 'event_create']);

Route::post('/admin/events/create', [AdminController::class, 'event_store']);

Route::get('/admin/events/event_list', [AdminController::class, 'event_show']);

Route::get('/admin/events/event_list/{id}', [AdminController::class, 'event_delete']);

Route::get('/admin/events/audience', [AdminController::class, 'audience']);

Route::get('/admin/events/audience', [AdminController::class, 'audience_view']);

Route::get('/admin/events/edit/{id}', [AdminController::class, 'event']);

Route::post('/admin/events/edit', [AdminController::class, 'event_edit']);

Route::get('/admin/events/edit/{id}', [AdminController::class, 'event_edit_view']);

Route::get('/admin/events/view/{id}', [AdminController::class, 'event_view']);

Route::get('/admin/events/audience_report', [AdminController::class, 'audience_report']);

Route::get('/admin/events/audience_edit/{id}', [AdminController::class, 'view_register']);

Route::post('/admin/events/audience_edit', [AdminController::class, 'update_register']);

Route::get('/admin/events/audience/{id}', [AdminController::class, 'delete_register']);

Route::get('/admin/events/view1', [AdminController::class, 'download']);

Route::get('/admin/events/view2', [AdminController::class, 'export']);

Route::get('/admin/events/view/{param}', [AdminController::class, 'slug']);

Route::post('/send-mail/{id}', [AdminController::class, 'sendemail']);

Route::get('/send-mail/{id}', [AdminController::class, 'sendemail']);



Route::get('/', function () {
    
});




Route::get('/booking', [HomeController::class, 'booking_view']);

Route::post('/booking', [HomeController::class, 'booking_create']);

Route::get('/booking/{id}', [HomeController::class, 'booking']);

Route::get('/event', [HomeController::class, 'event_view']);

Route::get('/event_details/{id}', [HomeController::class, 'event_details']);

Route::get('/booking_msg', [HomeController::class, 'booking_msg']);





